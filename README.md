# Wheels in Motion

## Authors
- [Ivan Pasportnikov](email@example.com)
- [Valentyna Rozhdsetvenska](rozhdval@student.cvut.cz)
- [Klim Schmelev](email@example.com)
- [Vladimir Mashkin](email@example.com)

## Technologies Used
- Bike Tracking System: GPS enabled tracking for all bikes.
- Mobile App: iOS and Android compatibility for booking and managing rentals.
- Payment System: Integration with major payment gateways for rental transactions.

## Description
"Wheels in Motion" is an innovative bike loan program designed to improve the lifestyle and health of our employees. By providing a long-term bike rental service, we aim to encourage physical activity and offer eco-friendly transportation options. This initiative aligns with our company's philosophy of supporting our employees' professional goals while fostering a balance between work and a healthy lifestyle.

## Installation
To install the "Wheels in Motion" app on your device, follow these steps:
1. Visit the App Store or Google Play.
2. Search for "Wheels in Motion".
3. Download and install the app.
4. Register with your company credentials to access the bike loan services.

## Copyright
© [2024] [Cycle'o]. All rights reserved. Unauthorized use and/or duplication of this material without express and written permission from the author and/or owner is strictly prohibited.
